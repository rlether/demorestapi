package br.com.rodrigo.demoRestApi.controller;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.rodrigo.demoRestApi.model.ComunsResponse;

@RestController
public class WebController {
	
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public ComunsResponse Test(@RequestParam(value="name", defaultValue="World") String name) {
		return new ComunsResponse(counter.incrementAndGet(), String.format(template, name));
	}

}
