# Demo Rest API

Projeto feito com Spring Boot, Nao consegui finalizar, deixei apenas um endpoint de test : GET : localhost:8080/test

## Rodar o Projeto

> Na Raiz do projeto

mvn spring-boot:run

Ira subir o spring boot no porta 8080

## Para Testar

curl -i -H "Accept: application/json" localhost:8080/test

Retorno:

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    34    0    34    0     0     34      0 --:--:-- --:--:-- --:--:--   274HTTP/1.1 200
Content-Type: application/json
Transfer-Encoding: chunked
Date: Wed, 13 Nov 2019 22:21:21 GMT

{"id":1,"message":"Hello, World!"}


Ou Pode testar em alguns ferramenta de mercado: Restlet / SoupUI / Postman 

GET : localhost:8080/test